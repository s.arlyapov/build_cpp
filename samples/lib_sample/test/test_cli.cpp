#define BOOST_TEST_MODULE test_main
#include <boost/test/unit_test.hpp>
#include <say/lib_hello.hpp>

BOOST_AUTO_TEST_SUITE(test_suite)

BOOST_AUTO_TEST_CASE(test_siple) {
    BOOST_CHECK(util::say_hello() == "Hello, from lib_hello!");
}

BOOST_AUTO_TEST_SUITE_END()