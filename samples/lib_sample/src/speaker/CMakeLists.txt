cmake_minimum_required(VERSION 3.9)
project(speaker VERSION 0.1.0)

include_directories(include ../utils/include)

add_executable(speaker main.cpp $<TARGET_OBJECTS:lib_hello>)
# target_link_libraries(speaker
# 	lib_hello
# )
install(TARGETS speaker RUNTIME DESTINATION bin)
