// #include "lib_hello.hpp"
#include <say/lib_hello.hpp>

namespace util {
std::string say_hello() {
    return "Hello, from lib_hello!";
}
}
