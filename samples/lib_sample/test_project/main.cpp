#include <iostream>
#include <say/lib_hello.hpp>
// #include "say/lib_hello.hpp"

int main() {
    std::cout << "Speaker says: " << util::say_hello() << std::endl;
    return 0;
}