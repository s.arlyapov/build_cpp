FROM gcc
LABEL maintainer "s.arlyapov@example.com"
RUN apt-get update
# SSH
RUN apt-get install -y openssh-server
RUN service ssh start
RUN mkdir /root/.ssh ; touch /root/.ssh/authorized_keys
ARG ZUKO
RUN if [ -n "${ZUKO}" ] ; then echo $ZUKO >> /root/.ssh/authorized_keys ; fi
ARG ASNB
RUN if [ -n "${ASNB}" ] ; then echo $ASNB >> /root/.ssh/authorized_keys ; fi
# DEBUG
RUN apt-get install -y strace ltrace gdb valgrind vim
# DEV
RUN apt-get install -y cmake libboost-test-dev python3-pip
ADD https://cmake.org/files/v3.15/cmake-3.15.2-Linux-x86_64.sh /cmake-3.15.2-Linux-x86_64.sh
RUN mkdir /opt/cmake
RUN sh /cmake-3.15.2-Linux-x86_64.sh --prefix=/opt/cmake --skip-license
RUN ln -s /opt/cmake/bin/cmake /usr/local/bin/cmake
RUN pip3 install conan
COPY samples /root/samples
#COPY entrypoint.sh /entrypoint.sh
#ENTRYPOINT ["/entrypoint.sh"]
