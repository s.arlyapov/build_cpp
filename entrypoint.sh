#!/bin/sh

service ssh start

if [ -n "ROOT_PASSWD" ]; then
	echo "root:${ROOT_PASSWD}" | chpasswd
fi

tail -f /dev/null
